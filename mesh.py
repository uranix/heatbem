from h2tools.collections.particles import Particles
import numpy as _np
import matplotlib.pyplot as _plt
from mpl_toolkits.mplot3d import Axes3D

class SurfaceMesh(Particles):
    @staticmethod
    def load(filename):
        with open(filename, 'r') as f:
            sig = f.readline()
            assert sig == 'surfacemesh\n'
            _npts = int(f.readline())
            pts = _np.empty((3, _npts))
            for i in range(_npts):
                xyz = f.readline()
                pts[:, i] = _np.double(xyz.split())
            ntri = int(f.readline())
            tri = _np.empty((3, ntri), dtype=_np.uint)
            for i in range(ntri):
                t = f.readline()
                tri[:, i] = _np.uint(t.split()) - 1
            edges = []
            for i in range(ntri):
                v1, v2, v3 = tri[:, i]
                swap = False
                if (v1, v2) in edges:
                    print 'dup', v1, v2
                    swap = True
                if (v2, v3) in edges:
                    print 'dup', v2, v3
                    swap = True
                if (v3, v1) in edges:
                    print 'dup', v3, v1
                    swap = True
                if swap:
                    print 'Triangle %d has wrong orientation' % i
                    v2,v3 = v3,v2
                    tri[:, i] = (v1,v2,v3)
                assert (v1, v2) not in edges
                assert (v2, v3) not in edges
                assert (v3, v1) not in edges
                edges.append((v1, v2))
                edges.append((v2, v3))
                edges.append((v3, v1))
            return SurfaceMesh(pts, tri)
    def __init__(self, pts, tri):
        self.pts = pts
        self.tri = tri
        assert tri.shape[0] == 3
        assert pts.shape[0] == 3
        ntri = tri.shape[1]
        _npts = pts.shape[1]
        colloc = _np.empty((3, ntri))
        for i in range(tri.shape[1]):
            colloc[:, i] = _np.mean(pts[:, tri[:, i]], axis=1)
        norm = _np.empty((3, ntri))
        S = _np.empty(ntri)
        for i in range(tri.shape[1]):
            p1, p2, p3 = pts[:, tri[:, i]].T
            norm[:, i] = _np.cross(p2 - p1, p3 - p1)
            S[i] = 0.5 * _np.linalg.norm(norm[:, i])
            norm[:, i] /= (2 * S[i])
        self.norm = norm
        self.S = S
        self.colloc = colloc
        super(SurfaceMesh, self).__init__(colloc.shape[0], colloc.shape[1], colloc)
    def show(self):
        fig = _plt.figure(figsize=(10,8))
        ax = fig.add_subplot(111, projection='3d')
        ax.view_init(elev=-68., azim=-26.)
        pts = self.pts
        tri = self.tri.T
        h = _np.mean(_np.sqrt(self.S))
        col = self.colloc
        nor = self.norm
        ax.plot_trisurf(pts[0,:], pts[1,:], pts[2,:], triangles=tri)
        ax.quiver(col[0,:], col[1,:], col[2,:], nor[0,:], nor[1,:], nor[2,:], color='r', length=0.1, pivot='tail')
        _plt.show()
    def plot(self, u):
        fig = _plt.figure(figsize=(10,8))
        ax = fig.add_subplot(111, projection='3d')
        ax.view_init(elev=-68., azim=-26.)
        pts = self.pts
        tri = self.tri.T
        h = _np.mean(_np.sqrt(self.S))
        col = self.colloc + h * self.norm
        foo = ax.plot_trisurf(pts[0,:], pts[1,:], pts[2,:], triangles=tri)
        foo.set_array(u)
        fig.colorbar(foo)
        _plt.show()

class VoxelNodes(Particles):
    def __init__(self, vox, idx):
        self.idx = idx.T
        super(VoxelNodes, self).__init__(vox.shape[0], vox.shape[1], vox)

class VoxelMesh(object):
    def __init__(self, Nx, Ny, Nz, hx, hy, hz, quadsplit=2):
        self.Nx, self.Ny, self.Nz = Nx, Ny, Nz
        self.shape = (Nx, Ny, Nz)
        self.hx, self.hy, self.hz = hx, hy, hz
        self.quadsplit = quadsplit
        f = _np.zeros((Nx + 2, Ny + 2, Nz + 2), dtype=_np.uint8)
        f[1:(Nx+1), 1:(Ny+1), 1:(Nz+1)] = 1
        self.f = f
        vox = []
        idx = []
        for i in range(Nx):
            for j in range(Ny):
                for k in range(Nz):
                    vox.append(self.center(i, j, k))
                    idx.append((i, j, k))
        vox = _np.array(vox).T
        idx = _np.array(idx).T
        self.vox = vox
        self.idx = idx
    def mask(self):
        Nx, Ny, Nz = self.Nx, self.Ny, self.Nz
        return _np.array([self[i,j,k] == 1 for i in range(Nx) for j in range(Ny) for k in range(Nz)])
    def quad(self, p1, p2, p3, p4, p5, reverse):
        if self.quadsplit == 4:
            if reverse:
                return [(p2, p1, p5), (p3, p2, p5), (p4, p3, p5), (p1, p4, p5)]
            return [(p1, p2, p5), (p2, p3, p5), (p3, p4, p5), (p4, p1, p5)]
        else:
            if reverse:
                return [(p2, p1, p4), (p3, p2, p4)]
            return [(p1, p2, p4), (p2, p3, p4)]
    def xface(self, i, j, k, reverse):
        p1 = (2*i, 2*j-2, 2*k-2)
        p2 = (2*i, 2*j  , 2*k-2)
        p3 = (2*i, 2*j  , 2*k  )
        p4 = (2*i, 2*j-2, 2*k  )
        p5 = (2*i, 2*j-1, 2*k-1)
        return self.quad(p1, p2, p3, p4, p5, reverse)
    def yface(self, i, j, k, reverse):
        p1 = (2*i-2, 2*j, 2*k-2)
        p2 = (2*i  , 2*j, 2*k-2)
        p3 = (2*i  , 2*j, 2*k  )
        p4 = (2*i-2, 2*j, 2*k  )
        p5 = (2*i-1, 2*j, 2*k-1)
        return self.quad(p1, p2, p3, p4, p5, reverse)
    def zface(self, i, j, k, reverse):
        p1 = (2*i-2, 2*j-2, 2*k)
        p2 = (2*i  , 2*j-2, 2*k)
        p3 = (2*i  , 2*j  , 2*k)
        p4 = (2*i-2, 2*j  , 2*k)
        p5 = (2*i-1, 2*j-1, 2*k)
        return self.quad(p1, p2, p3, p4, p5, reverse)
    def voxelNodes(self):
        mask = self.mask()
        vox = self.vox[:, mask]
        idx = self.idx[:, mask]
        return VoxelNodes(vox, idx)
    def voxelSurface(self):
        f = self.f
        Nx, Ny, Nz = self.Nx, self.Ny, self.Nz
        # x pass
        res = []
        for i in range(0, Nx+1):
            for j in range(1, Ny+1):
                for k in range(1, Nz+1):
                    if f[i, j, k] == f[i+1, j, k]:
                        continue
                    res += self.xface(i, j, k, f[i, j, k] == 0)
        for j in range(0, Ny+1):
            for i in range(1, Nx+1):
                for k in range(1, Nz+1):
                    if f[i, j, k] == f[i, j+1, k]:
                        continue
                    res += self.yface(i, j, k, f[i, j, k] != 0)
        for k in range(0, Nz+1):
            for j in range(1, Ny+1):
                for i in range(1, Nx+1):
                    if f[i, j, k] == f[i, j, k+1]:
                        continue
                    res += self.zface(i, j, k, f[i, j, k] == 0)
        pts = {}
        steps = 0.5 * _np.array([self.hx, self.hy, self.hz])
        for (a, b, c) in res:
            pts[a] = steps * _np.array(a)
            pts[b] = steps * _np.array(b)
            pts[c] = steps * _np.array(c)
        ptsidx = pts.keys()
        ptscoord = pts.values()
        idxpts = dict(zip(ptsidx, range(len(ptsidx))))
        tri = []
        for (a, b, c) in res:
            tri.append([idxpts[a], idxpts[b], idxpts[c]])
        return SurfaceMesh(_np.array(ptscoord).T, _np.array(tri, dtype=_np.uint).T)
    def __getitem__(self, key):
        i, j, k = key
        return self.f[i+1, j+1, k+1]
    def __setitem__(self, key, val):
        i, j, k = key
        self.f[i+1, j+1, k+1] = val
    def save(self, filename, u, gu):
        from evtk.hl import gridToVTK 
        
        Nx, Ny, Nz = self.Nx, self.Ny, self.Nz
        ncells = Nx * Ny * Nz
        npoints = (Nx + 1) * (Ny + 1) * (Nz + 1) 

        X = _np.arange(Nx + 1) * self.hx 
        Y = _np.arange(Ny + 1) * self.hy 
        Z = _np.arange(Nz + 1) * self.hz 

        x = _np.zeros((Nx + 1, Ny + 1, Nz + 1))
        y = _np.zeros((Nx + 1, Ny + 1, Nz + 1))
        z = _np.zeros((Nx + 1, Ny + 1, Nz + 1))
        
        for k in range(Nz + 1): 
            for j in range(Ny + 1):
                for i in range(Nx + 1): 
                    x[i,j,k] = X[i]
                    y[i,j,k] = Y[j] 
                    z[i,j,k] = Z[k]

        mask = self.mask()
        U = _np.zeros(Nx * Ny * Nz)
        G = _np.zeros([Nx * Ny * Nz, 3])
        U[mask] = u
        G[mask] = gu
        U = U.reshape([Nx, Ny, Nz])
        G = G.reshape([Nx, Ny, Nz, 3])
        Gx, Gy, Gz = G[:, :, :, 0], G[:, :, :, 1], G[:, :, :, 2]
        Gx = Gx.reshape([Nx, Ny, Nz]).copy()
        Gy = Gy.reshape([Nx, Ny, Nz]).copy()
        Gz = Gz.reshape([Nx, Ny, Nz]).copy()
        gridToVTK(filename, x, y, z, cellData = {"u" : U, "g" : (Gx, Gy, Gz), "f" : mask.astype(_np.int)})
    def center(self, i, j, k):
        return _np.array([(i + 0.5) * self.hx, (j + 0.5) * self.hy, (k + 0.5) * self.hz])
