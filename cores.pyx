#!python
#cython: boundscheck=False

import numpy as np
cimport numpy as np

cdef extern from *:
#     ctypedef int quadPointsTri "85"
#     ctypedef int quadPointsLin "40"

# Possible values for triange - 1, 7, 25, 54, 85
    ctypedef int quadPointsTri "25"
    ctypedef int quadPointsLin "40"

cdef extern from "iequad/iequad.h" namespace "iequad":
    cdef cppclass point:
        double x, y, z
        point()
        point(double x, double y, double z)
        double dot(point &other)
    R singular_tri[R,Q,K](K &kernel, point &x, point &p1, point &p2, point &p3)
    R regular_tri[R,Q,K](K &kernel, point &x, point &p1, point &p2, point &p3)

cdef extern from "kernels.h":
    cdef cppclass DoubleLayer:
        DoubleLayer()
        DoubleLayer(point &n)

cdef extern from "kernels.h":
    cdef cppclass GradDoubleLayer:
        GradDoubleLayer()
        GradDoubleLayer(point &n)

cdef extern from "kernels.h":
    cdef cppclass SimpleLayer:
        SimpleLayer()

cdef extern from "kernels.h":
    cdef cppclass GradSimpleLayer:
        GradSimpleLayer()

cdef extern from "kernels.h":
    cdef cppclass ten:
        double tr
        point skew

cdef extern from "kernels.h":
    cdef cppclass GenericDoubleLayer:
        GenericDoubleLayer()
        GenericDoubleLayer(point &n)

def Kcore(np.ndarray[double, ndim=2] pts, np.ndarray[unsigned long, ndim=2] tri,
               np.ndarray[double, ndim=2] colloc, np.ndarray[double, ndim=2] norm,
               np.ndarray[unsigned long, ndim=1] rows, np.ndarray[unsigned long, ndim=1] cols):
    cdef np.ndarray[double, ndim=2] a = np.ndarray([rows.shape[0], cols.shape[0]])
    cdef unsigned long i, j
    cdef unsigned long v1, v2, v3
    cdef unsigned long dst, src

    cdef point n
    cdef point p1
    cdef point p2
    cdef point p3
    cdef point x

    cdef DoubleLayer dl

    for j in range(<unsigned long>(cols.shape[0])):
        src = cols[j]
        v1 = tri[0, src]
        v2 = tri[1, src]
        v3 = tri[2, src]
        n = point(norm[0, src], norm[1, src], norm[2, src])
        dl = DoubleLayer(n)
        p1 = point(pts[0, v1], pts[1, v1], pts[2, v1])
        p2 = point(pts[0, v2], pts[1, v2], pts[2, v2])
        p3 = point(pts[0, v3], pts[1, v3], pts[2, v3])
        for i in range(<unsigned long>(rows.shape[0])):
            dst = rows[i]
            x = point(colloc[0, dst], colloc[1, dst], colloc[2, dst])
            if src == dst:
                a[i, j] = singular_tri[double,quadPointsLin,DoubleLayer](dl, x, p1, p2, p3)
            else:
                a[i, j] = regular_tri[double,quadPointsTri,DoubleLayer](dl, x, p1, p2, p3)
    return a

def Vcore(np.ndarray[double, ndim=2] pts, np.ndarray[unsigned long, ndim=2] tri,
               np.ndarray[double, ndim=2] colloc,
               np.ndarray[unsigned long, ndim=1] rows, np.ndarray[unsigned long, ndim=1] cols):
    cdef np.ndarray[double, ndim=2] a = np.ndarray([rows.shape[0], cols.shape[0]])
    cdef unsigned long i, j
    cdef unsigned long v1, v2, v3
    cdef unsigned long dst, src

    cdef point p1
    cdef point p2
    cdef point p3
    cdef point x

    cdef SimpleLayer sl

    for j in range(<unsigned long>(cols.shape[0])):
        src = cols[j]
        v1 = tri[0, src]
        v2 = tri[1, src]
        v3 = tri[2, src]
        p1 = point(pts[0, v1], pts[1, v1], pts[2, v1])
        p2 = point(pts[0, v2], pts[1, v2], pts[2, v2])
        p3 = point(pts[0, v3], pts[1, v3], pts[2, v3])
        for i in range(<unsigned long>(rows.shape[0])):
            dst = rows[i]
            x = point(colloc[0, dst], colloc[1, dst], colloc[2, dst])
            if src == dst:
                a[i, j] = singular_tri[double,quadPointsLin,SimpleLayer](sl, x, p1, p2, p3)
            else:
                a[i, j] = regular_tri[double,quadPointsTri,SimpleLayer](sl, x, p1, p2, p3)
    return a

def gradKcore(np.ndarray[double, ndim=2] pts, np.ndarray[unsigned long, ndim=2] tri,
               np.ndarray[double, ndim=2] vox, np.ndarray[double, ndim=2] norm,
               np.ndarray[unsigned long, ndim=1] rows, np.ndarray[unsigned long, ndim=1] cols):
    cdef np.ndarray[double, ndim=3] a = np.ndarray([rows.shape[0], 3, cols.shape[0]])
    cdef unsigned long i, j
    cdef unsigned long v1, v2, v3
    cdef unsigned long dst, src

    cdef point n
    cdef point p1
    cdef point p2
    cdef point p3
    cdef point x
    cdef point v

    cdef GradDoubleLayer dl

    for j in range(<unsigned long>(cols.shape[0])):
        src = cols[j]
        v1 = tri[0, src]
        v2 = tri[1, src]
        v3 = tri[2, src]
        n  = point(norm[0, src], norm[1, src], norm[2, src])
        dl = GradDoubleLayer(n)
        p1 = point(pts[0, v1], pts[1, v1], pts[2, v1])
        p2 = point(pts[0, v2], pts[1, v2], pts[2, v2])
        p3 = point(pts[0, v3], pts[1, v3], pts[2, v3])
        for i in range(<unsigned long>(rows.shape[0])):
            dst = rows[i]
            x = point(vox[0, dst], vox[1, dst], vox[2, dst])
            v = regular_tri[point,quadPointsTri,GradDoubleLayer](dl, x, p1, p2, p3)
            a[i, 0, j] = v.x
            a[i, 1, j] = v.y
            a[i, 2, j] = v.z
    return a

def gradVcore(np.ndarray[double, ndim=2] pts, np.ndarray[unsigned long, ndim=2] tri,
               np.ndarray[double, ndim=2] vox,
               np.ndarray[unsigned long, ndim=1] rows, np.ndarray[unsigned long, ndim=1] cols):
    cdef np.ndarray[double, ndim=3] a = np.ndarray([rows.shape[0], 3, cols.shape[0]])
    cdef unsigned long i, j
    cdef unsigned long v1, v2, v3
    cdef unsigned long dst, src

    cdef point p1
    cdef point p2
    cdef point p3
    cdef point x
    cdef point v

    cdef GradSimpleLayer sl

    for j in range(<unsigned long>(cols.shape[0])):
        src = cols[j]
        v1 = tri[0, src]
        v2 = tri[1, src]
        v3 = tri[2, src]
        p1 = point(pts[0, v1], pts[1, v1], pts[2, v1])
        p2 = point(pts[0, v2], pts[1, v2], pts[2, v2])
        p3 = point(pts[0, v3], pts[1, v3], pts[2, v3])
        for i in range(<unsigned long>(rows.shape[0])):
            dst = rows[i]
            x = point(vox[0, dst], vox[1, dst], vox[2, dst])
            v = regular_tri[point,quadPointsTri,GradSimpleLayer](sl, x, p1, p2, p3)
            a[i, 0, j] = v.x
            a[i, 1, j] = v.y
            a[i, 2, j] = v.z
    return a

def volKcore(np.ndarray[double, ndim=2] pts, np.ndarray[unsigned long, ndim=2] tri,
               np.ndarray[double, ndim=2] vox, np.ndarray[double, ndim=2] norm,
               np.ndarray[unsigned long, ndim=1] rows, np.ndarray[unsigned long, ndim=1] cols):
    cdef np.ndarray[double, ndim=2] a = np.ndarray([rows.shape[0], cols.shape[0]])
    cdef unsigned long i, j
    cdef unsigned long v1, v2, v3
    cdef unsigned long dst, src

    cdef point n
    cdef point p1
    cdef point p2
    cdef point p3
    cdef point x

    cdef DoubleLayer dl

    for j in range(<unsigned long>(cols.shape[0])):
        src = cols[j]
        v1 = tri[0, src]
        v2 = tri[1, src]
        v3 = tri[2, src]
        n  = point(norm[0, src], norm[1, src], norm[2, src])
        dl = DoubleLayer(n)
        p1 = point(pts[0, v1], pts[1, v1], pts[2, v1])
        p2 = point(pts[0, v2], pts[1, v2], pts[2, v2])
        p3 = point(pts[0, v3], pts[1, v3], pts[2, v3])
        for i in range(<unsigned long>(rows.shape[0])):
            dst = rows[i]
            x = point(vox[0, dst], vox[1, dst], vox[2, dst])
            a[i, j] = regular_tri[double,quadPointsTri,DoubleLayer](dl, x, p1, p2, p3)
    return a

def volVcore(np.ndarray[double, ndim=2] pts, np.ndarray[unsigned long, ndim=2] tri,
               np.ndarray[double, ndim=2] vox,
               np.ndarray[unsigned long, ndim=1] rows, np.ndarray[unsigned long, ndim=1] cols):
    cdef np.ndarray[double, ndim=2] a = np.ndarray([rows.shape[0], cols.shape[0]])
    cdef unsigned long i, j
    cdef unsigned long v1, v2, v3
    cdef unsigned long dst, src

    cdef point p1
    cdef point p2
    cdef point p3
    cdef point x

    cdef SimpleLayer sl

    for j in range(<unsigned long>(cols.shape[0])):
        src = cols[j]
        v1 = tri[0, src]
        v2 = tri[1, src]
        v3 = tri[2, src]
        p1 = point(pts[0, v1], pts[1, v1], pts[2, v1])
        p2 = point(pts[0, v2], pts[1, v2], pts[2, v2])
        p3 = point(pts[0, v3], pts[1, v3], pts[2, v3])
        for i in range(<unsigned long>(rows.shape[0])):
            dst = rows[i]
            x = point(vox[0, dst], vox[1, dst], vox[2, dst])
            a[i, j] = regular_tri[double,quadPointsTri,SimpleLayer](sl, x, p1, p2, p3)
    return a

def Gcore(np.ndarray[double, ndim=2] pts, np.ndarray[unsigned long, ndim=2] tri,
               np.ndarray[double, ndim=2] colloc, np.ndarray[double, ndim=2] norm,
               np.ndarray[unsigned long, ndim=1] rows, np.ndarray[unsigned long, ndim=1] cols):
    cdef np.ndarray[double, ndim=3] a = np.ndarray([rows.shape[0], 4, cols.shape[0]])
    cdef unsigned long i, j
    cdef unsigned long v1, v2, v3
    cdef unsigned long dst, src

    cdef point n
    cdef point p1
    cdef point p2
    cdef point p3
    cdef point x

    cdef GenericDoubleLayer dl

    cdef ten v

    for j in range(<unsigned long>(cols.shape[0])):
        src = cols[j]
        v1 = tri[0, src]
        v2 = tri[1, src]
        v3 = tri[2, src]
        n = point(norm[0, src], norm[1, src], norm[2, src])
        dl = GenericDoubleLayer(n)
        p1 = point(pts[0, v1], pts[1, v1], pts[2, v1])
        p2 = point(pts[0, v2], pts[1, v2], pts[2, v2])
        p3 = point(pts[0, v3], pts[1, v3], pts[2, v3])
        for i in range(<unsigned long>(rows.shape[0])):
            dst = rows[i]
            x = point(colloc[0, dst], colloc[1, dst], colloc[2, dst])
            if src == dst:
                v = singular_tri[ten,quadPointsLin,GenericDoubleLayer](dl, x, p1, p2, p3)
            else:
                v =  regular_tri[ten,quadPointsTri,GenericDoubleLayer](dl, x, p1, p2, p3)
            a[i, 0, j] = v.tr
            a[i, 1, j] = v.skew.x
            a[i, 2, j] = v.skew.y
            a[i, 3, j] = v.skew.z
    return a