#include "iequad/point.h"

struct DoubleLayer {
    static constexpr bool strongSingular = true;
    iequad::point n;
    DoubleLayer(const iequad::point &n) : n(n) { }
    DoubleLayer() { }
    DoubleLayer &operator=(DoubleLayer &&) = default;
    double operator()(const iequad::point &y_x, const double r) const {
        return -0.07957747154594767 * y_x.dot(n) / (r * r * r) + 1e-30 / r;
    }
};

struct SimpleLayer {
    static constexpr bool strongSingular = false;
    SimpleLayer() { }
    SimpleLayer &operator=(SimpleLayer &&) = default;
    double operator()(const iequad::point &, const double r) const {
        return 0.07957747154594767 / r;
    }
};

struct GradDoubleLayer {
    iequad::point n;
    GradDoubleLayer(const iequad::point &n) : n(n) { }
    GradDoubleLayer() { }
    GradDoubleLayer &operator=(GradDoubleLayer &&) = default;
    iequad::point operator()(const iequad::point &y_x, const double r) const {
        double r2 = r * r;
        return (n * r2 - y_x * (3 * y_x.dot(n))) * (0.07957747154594767 / (r * r2 * r2)) + iequad::point(1, 1, 1) * (1e-30 / r);
    }
};

struct GradSimpleLayer {
    GradSimpleLayer() { }
    GradSimpleLayer &operator=(GradSimpleLayer &&) = default;
    iequad::point operator()(const iequad::point &y_x, const double r) const {
        return y_x * (0.07957747154594767 / (r * r * r));
    }
};

struct ten {
    double tr;
    iequad::point skew;
    const ten operator*(const double m) const {
        return ten{tr * m, skew * m};
    }
    const ten operator+(const ten &o) const {
        return ten{tr + o.tr, skew + o.skew};
    }
};

struct GenericDoubleLayer {
    static constexpr bool strongSingular = true;
    iequad::point n;
    GenericDoubleLayer(const iequad::point &n) : n(n) { }
    GenericDoubleLayer() { }
    GenericDoubleLayer &operator=(GenericDoubleLayer &&) = default;
    ten operator()(const iequad::point &y_x, const double r) const {
        const double m = -0.07957747154594767 / (r * r * r);
        return ten{y_x.dot(n), y_x.cross(n)} * m;
    }
};
