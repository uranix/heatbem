import numpy as _np

import pyximport
pyximport.install()
from cores import *

full = True

from h2tools import ClusterTree, Problem
from h2tools.collections.particles import Particles
from h2tools.mcbh import mcbh
from h2tools.h2matrix import H2matrix

__all__ = ['diffnormest', 'surfKmatrix', 'surfGmatrix', 'surfVmatrix', 'volKmatrix', 'volVmatrix', 'gradKmatrix', 'gradVmatrix']

def _surfKfunc(row_data, rows, col_data, cols):
    return Kcore(row_data.pts, row_data.tri, row_data.colloc, row_data.norm,
                 rows.astype(_np.uint), cols.astype(_np.uint))

def _surfGfunc(row_data, rows, col_data, cols):
    return Gcore(row_data.pts, row_data.tri, row_data.colloc, row_data.norm,
                 rows.astype(_np.uint), cols.astype(_np.uint))

def _surfVfunc(row_data, rows, col_data, cols):
    return Vcore(row_data.pts, row_data.tri, row_data.colloc,
                 rows.astype(_np.uint), cols.astype(_np.uint))

def _gradKfunc(row_data, rows, col_data, cols):
    return gradKcore(col_data.pts, col_data.tri, row_data.vertex, col_data.norm,
                    rows.astype(_np.uint), cols.astype(_np.uint))

def _gradVfunc(row_data, rows, col_data, cols):
    return gradVcore(col_data.pts, col_data.tri, row_data.vertex,
                    rows.astype(_np.uint), cols.astype(_np.uint))

def _volKfunc(row_data, rows, col_data, cols):
    return volKcore(col_data.pts, col_data.tri, row_data.vertex, col_data.norm,
                    rows.astype(_np.uint), cols.astype(_np.uint))

def _volVfunc(row_data, rows, col_data, cols):
    return volVcore(col_data.pts, col_data.tri, row_data.vertex,
                    rows.astype(_np.uint), cols.astype(_np.uint))

def _surfKmatrixMCBH(s, tau=1e-4, iters=3, block_size=20):
    tree = ClusterTree(s, block_size=block_size)
    problem = Problem(_surfKfunc, tree, tree, symmetric=False, verbose=False)
    return mcbh(problem, tau=tau, iters=iters, verbose=True)

def _surfGmatrixMCBH(s, tau=1e-4, iters=1, block_size=20):
    tree = ClusterTree(s, block_size=block_size)
    problem = Problem(_surfGfunc, tree, tree, symmetric=False, verbose=False)
    return mcbh(problem, tau=tau, iters=iters, verbose=True)

def _surfVmatrixMCBH(s, tau=1e-4, iters=1, block_size=20):
    tree = ClusterTree(s, block_size=block_size)
    problem = Problem(_surfVfunc, tree, tree, symmetric=False, verbose=False)
    return mcbh(problem, tau=tau, iters=iters, verbose=True)

def _gradKmatrixMCBH(v, s, tau=1e-4, iters=1, block_size=20):
    coltree = ClusterTree(s, block_size=block_size)
    rowtree = ClusterTree(v, block_size=block_size)
    problem = Problem(_gradKfunc, rowtree, coltree, symmetric=False, verbose=False)
    return mcbh(problem, tau=tau, iters=iters, verbose=True, random_init=block_size)

def _gradVmatrixMCBH(v, s, tau=1e-4, iters=1, block_size=20):
    coltree = ClusterTree(s, block_size=block_size)
    rowtree = ClusterTree(v, block_size=block_size)
    problem = Problem(_gradVfunc, rowtree, coltree, symmetric=False, verbose=False)
    return mcbh(problem, tau=tau, iters=iters, verbose=True, random_init=block_size)

def _volKmatrixMCBH(v, s, tau=1e-4, iters=1, block_size=20):
    coltree = ClusterTree(s, block_size=block_size)
    rowtree = ClusterTree(v, block_size=block_size)
    problem = Problem(_volKfunc, rowtree, coltree, symmetric=False, verbose=False)
    return mcbh(problem, tau=tau, iters=iters, verbose=True, random_init=block_size)

def _volVmatrixMCBH(v, s, tau=1e-4, iters=1, block_size=20):
    coltree = ClusterTree(s, block_size=block_size)
    rowtree = ClusterTree(v, block_size=block_size)
    problem = Problem(_volVfunc, rowtree, coltree, symmetric=False, verbose=False)
    return mcbh(problem, tau=tau, iters=iters, verbose=True, random_init=block_size)

def _surfKmatrixFull(s):
    n = len(s)
    return _surfKfunc(s, _np.arange(n, dtype=_np.uint), s, _np.arange(n, dtype=_np.uint))

def _surfGmatrixFull(s):
    n = len(s)
    return _surfGfunc(s, _np.arange(n, dtype=_np.uint), s, _np.arange(n, dtype=_np.uint))

def _surfVmatrixFull(s):
    n = len(s)
    return _surfVfunc(s, _np.arange(n, dtype=_np.uint), s, _np.arange(n, dtype=_np.uint))

def _gradKmatrixFull(v, s):
    n = len(s)
    m = len(v)
    return _gradKfunc(v, _np.arange(m, dtype=_np.uint), s, _np.arange(n, dtype=_np.uint))

def _gradVmatrixFull(v, s):
    n = len(s)
    m = len(v)
    return _gradVfunc(v, _np.arange(m, dtype=_np.uint), s, _np.arange(n, dtype=_np.uint))

def _volKmatrixFull(v, s):
    n = len(s)
    m = len(v)
    return _volKfunc(v, _np.arange(m, dtype=_np.uint), s, _np.arange(n, dtype=_np.uint))

def _volVmatrixFull(v, s):
    n = len(s)
    m = len(v)
    return _volVfunc(v, _np.arange(m, dtype=_np.uint), s, _np.arange(n, dtype=_np.uint))

def surfKmatrix(s, **kwargs):
    if full:
        return _surfKmatrixFull(s, **kwargs)
    else:
        return _surfKmatrixMCBH(s, **kwargs)
    
def surfGmatrix(s, **kwargs):
    if full:
        return _surfGmatrixFull(s, **kwargs)
    else:
        return _surfGmatrixMCBH(s, **kwargs)

def surfVmatrix(s, **kwargs):
    if full:
        return _surfVmatrixFull(s, **kwargs)
    else:
        return _surfVmatrixMCBH(s, **kwargs)

def gradKmatrix(v, s, **kwargs):
    if full:
        return _gradKmatrixFull(v, s, **kwargs)
    else:
        return _gradKmatrixMCBH(v, s, **kwargs)

def gradVmatrix(v, s, **kwargs):
    if full:
        return _gradVmatrixFull(v, s, **kwargs)
    else:
        return _gradVmatrixMCBH(v, s, **kwargs)

def volKmatrix(v, s, **kwargs):
    if full:
        return _volKmatrixFull(v, s, **kwargs)
    else:
        return _volKmatrixMCBH(v, s, **kwargs)

def volVmatrix(v, s, **kwargs):
    if full:
        return _volVmatrixFull(v, s, **kwargs)
    else:
        return _volVmatrixMCBH(v, s, **kwargs)

def diffnormest(M):
    x = _np.random.rand(M.shape[1])
    yd = M.problem.dot(x)
    yh = M.dot(x)
    return _np.linalg.norm(yd - yh) / _np.linalg.norm(yd)